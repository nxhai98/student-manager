package com.hainx.student_manager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;

import com.hainx.student_manager.SQLite.DatabaseHandler;
import com.hainx.student_manager.student.StudentAdapter;
import com.hainx.student_manager.student.StudentModel;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listStudent;
    SearchView txtSearchView;
    List<StudentModel> studentModelList;
    StudentAdapter studentAdapter;
    SQLiteDatabase db;
    File storagePath;
    String studentDatabasePath;
    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listStudent = findViewById(R.id.list_student);
        listStudent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, InfoActivity.class);
                intent.putExtra("ACTION", "change");
                intent.putExtra("ID", studentModelList.get(position).getMssv());
                startActivity(intent);
            }
        });
        registerForContextMenu(listStudent);
        studentModelList = new ArrayList<>();

        // set database path
        storagePath = getApplication().getFilesDir();
        studentDatabasePath = storagePath + "/studentDB";
        databaseHandler = new DatabaseHandler(this);

        // test data
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        txtSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        txtSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // search
                if (query.isEmpty()) {
                    loadData();
                    return false;
                }
                studentModelList = databaseHandler.search(query);
                loadDataSearch();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//                databaseHandler.search()
                return false;
            }
        });
        txtSearchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                loadData();
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_add) {
            // them moi
            Intent intent = new Intent(MainActivity.this, InfoActivity.class);
            intent.putExtra("ACTION", "add");
            startActivity(intent);
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        ListView lv = (ListView)v;
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
        StudentModel studentModel = (StudentModel)lv.getItemAtPosition(acmi.position);
        menu.setHeaderTitle("Choose Option");
        menu.add(0, studentModel.getMssv(), 0, "Change");
        menu.add(0, studentModel.getMssv(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        if (item.getTitle() == "Change") {
            Intent intent = new Intent(MainActivity.this, InfoActivity.class);
            intent.putExtra("ACTION", "change");
            intent.putExtra("ID", item.getItemId());
            startActivity(intent);
        } else if (item.getTitle() == "Delete") {
            int id = item.getItemId();
            createDeleteAlertDialog(this, id);
            loadData();
        } else {
            return false;
        }
        return true;
    }

    private void loadData() {
        studentModelList = databaseHandler.getAllStudents();
        studentAdapter = new StudentAdapter(studentModelList);
        listStudent.setAdapter(studentAdapter);
    }

    private void loadDataSearch() {
        studentAdapter = new StudentAdapter(studentModelList);
        listStudent.setAdapter(studentAdapter);
    }


    private void createDeleteAlertDialog(MainActivity mainActivity, final int id) {
        new AlertDialog.Builder(mainActivity)
                .setTitle("Confirm")
                .setMessage("Are you sure to delete " + id)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // delete this chose
                        databaseHandler.deleteStudent(id);
                        loadData();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create()
                .show();
    }
}
