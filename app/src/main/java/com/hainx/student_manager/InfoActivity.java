package com.hainx.student_manager;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hainx.student_manager.SQLite.DatabaseHandler;
import com.hainx.student_manager.student.StudentModel;

public class InfoActivity extends AppCompatActivity {
    String action = "";
    Button btnDone;
    DatabaseHandler databaseHandler;
    int currentStudentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        action = getIntent().getStringExtra("ACTION");
        databaseHandler = new DatabaseHandler(this);
        if (action.equals("change")) {
            currentStudentId = getIntent().getIntExtra("ID" ,0);
            if (currentStudentId != 0)
                setData(databaseHandler.getStudent(currentStudentId));
            ((TextView)findViewById(R.id.edt_mssv)).setEnabled(false);
        }
        btnDone = findViewById(R.id.btnDone);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    if (action.equals("change")) {
                        databaseHandler.updateStudent(getInfo());
                    } else if (action.equals("add")) {
                        databaseHandler.addStudent(getInfo());
                    }
                    finish();
                } else {
                    Toast.makeText(InfoActivity.this, "Dien day du vao", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private StudentModel getInfo() {
        StudentModel temp = new StudentModel();
        temp.setMssv(getIntOf(R.id.edt_mssv));
        temp.setName(getStringOf(R.id.edt_name));
        temp.setBirth(getStringOf(R.id.edt_birth));
        temp.setCountry(getStringOf(R.id.edt_country));
        temp.setEmail(getStringOf(R.id.edt_email));
        return temp;
    }

    private int getIntOf(int id) {
        TextView tv = findViewById(id);
        int res = 0;
        try {
            res = Integer.valueOf(tv.getText().toString());
        } catch (Exception e) {

        }
        return res;
    }

    private String getStringOf(int id) {
        TextView tv = findViewById(id);
        return tv.getText().toString();
    }

    private boolean check() {
        if ((getStringOf(R.id.edt_mssv)).trim().isEmpty() |
                (getStringOf(R.id.edt_name)).trim().isEmpty() |
                (getStringOf(R.id.edt_birth)).trim().isEmpty() |
                (getStringOf(R.id.edt_country)).trim().isEmpty() |
                (getStringOf(R.id.edt_email)).trim().isEmpty() ) {
            return false;
        };
        return true;
    }

    private void setData(StudentModel studentModel) {
        setDataTo(R.id.edt_mssv, String.valueOf(studentModel.getMssv()));
        setDataTo(R.id.edt_name, studentModel.getName());
        setDataTo(R.id.edt_birth, studentModel.getBirth());
        setDataTo(R.id.edt_country, studentModel.getCountry());
        setDataTo(R.id.edt_email, studentModel.getEmail());
    }

    private void setDataTo(int id, String data) {
        ((TextView)findViewById(id)).setText(data);
    }
}
