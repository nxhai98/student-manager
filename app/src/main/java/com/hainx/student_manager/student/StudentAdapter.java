package com.hainx.student_manager.student;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hainx.student_manager.R;

import java.util.List;

public class StudentAdapter extends BaseAdapter {

    List<StudentModel> studentModelList;

    public StudentAdapter(List<StudentModel> studentModelList) {
        this.studentModelList = studentModelList;
    }

    @Override
    public int getCount() {
        return studentModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return studentModelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return studentModelList.get(position).getMssv();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder viewHolder;
        if (view == null) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_item, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.studentEmail = view.findViewById(R.id.student_email);
            viewHolder.studentID = view.findViewById(R.id.student_id);
            viewHolder.studentName = view.findViewById(R.id.student_name);

            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder)view.getTag();
        }

        final StudentModel studentModel = studentModelList.get(position);

        viewHolder.studentName.setText(studentModel.getName());
        viewHolder.studentID.setText(String.valueOf(studentModel.getMssv()));
        viewHolder.studentEmail.setText(studentModel.getEmail());

        return view;
    }

    class ViewHolder {
        TextView studentID;
        TextView studentName;
        TextView studentEmail;
    }
}
