package com.hainx.student_manager.student;

public class StudentModel {
    private int mssv;
    private String name;
    private String birth;
    private String email;
    private String country;

    public StudentModel() {
    }

    public StudentModel(int mssv, String name, String birth, String email, String country) {
        this.mssv = mssv;
        this.name = name;
        this.birth = birth;
        this.email = email;
        this.country = country;
    }

    public int getMssv() {
        return mssv;
    }

    public void setMssv(int mssv) {
        this.mssv = mssv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
